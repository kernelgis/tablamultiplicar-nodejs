const fs = require('fs');

function crearTabla(base) {
  return new Promise((resolve, reject) => {

    if(!Number(base)){
      reject(`la base ${base} no es un Número`);
      return;
    }
    
    let data = '';
    console.log(`Tabla de Multiplicar del ${base}`);
    
    for (let i = 0; i < 10; i++) {
      data += `${base} * ${i} = ${base * i}\n`;
      console.log(`${base} * ${i} = ${base * i}`);
    }
    
    fs.writeFile(`Tabla-${base}.txt`, data, (err) => {
      if (err) reject(err);
      else
        resolve(`Tabla-${base}.txt`)
      // console.log('Archivo Creado');
    });
  });
}

function listar(nombreArchivo) {

}


module.exports = {
  crearTabla,
  listar
}




