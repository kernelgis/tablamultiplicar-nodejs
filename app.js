// const multiplicar = require('./tablas/multiplicar');
const { crearTabla } = require('./tablas/multiplicar');

let base = 5;

crearTabla(base)
  .then(r => {
    console.log(r);
  })
  .catch(e => {
    console.error(e);
  });
